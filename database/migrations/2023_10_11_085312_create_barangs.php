<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->id();
            $table->integer('pegawai_id')->nullable();
            $table->string('nomerurut');
            $table->string('serialnumber');
            $table->string('name');
            $table->string('merek');
            $table->string('ruangan');
            $table->string('model');
            $table->string('harga');
            $table->date('tanggal_pengadaan');
            $table->string('kondisi');
            $table->string('gambar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('barangs');
    }
};
