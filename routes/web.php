<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', App\Livewire\Dashboard::class)->name('dashboard');
    Route::get('/product', App\Livewire\Product::class)->name('product');
    Route::get('/pegawai', App\Livewire\Pegawai::class)->name('pegawai');
    Route::get('/admin', App\Livewire\Admin::class)->name('admin');

    Route::get('/excel_product', [App\Http\Controllers\ProductController::class, 'export'])->name('export-prodcut-excel');
    Route::get('/export_qr/{id}', [App\Http\Controllers\ProductController::class, 'qr'])->name('export-qr');
    Route::get('/export_barcode/{id}', [App\Http\Controllers\ProductController::class, 'barcode'])->name('export-barcode');
});


Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false
]);
