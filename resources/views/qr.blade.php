<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div class="ggwp">
    <h1>
        QR Code
    </h1>
    {!! \QrCode::size(300)->generate($qr) !!}
    <br>
</div>

<script>
    const source = window.document.getElementsByClassName("ggwp")[0];
    html2pdf().from(source).save();

    setTimeout(() => {
        window.close();
    }, 2500);
</script>
