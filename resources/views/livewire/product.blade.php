<div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title ?? '' }}
        </h1>
    </section>

    <!-- Main content -->
    <section class="content" style="margin-top: 50px">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $title ?? '' }}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <x-acc-header>
                            <div style="row">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="{{ route('export-prodcut-excel') }}" target="_blank" class="btn btn-primary">
                                        Export Excel
                                    </a>
                                </div>
                            </div>
                        </x-acc-header>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <x-acc-loop-th :$searchBy :$orderBy :$order />
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                @forelse($get as $d)
                                    <tr>
                                        <td>{{ $d->pegawai }}</td>
                                        <td>{{ $d->nomerurut }}</td>
                                        <td>
                                            {{ $d->serialnumber }}
                                            <br>
                                            <button
                                                x-data
                                                @click="$('#showqr-modal').modal('toggle');"
                                                wire:click="generateQR({{ $d->id }})">Generate QRCode</button>
                                        </td>
                                        <td>{{ $d->name }}</td>
                                        <td>{{ $d->merek }}</td>
                                        <td>{{ $d->model }}</td>
                                        <td>{{ $d->ruangan }}</td>
                                        <td>{{ $d->kondisi }}</td>
                                        <td>{{ $d->vendor }}</td>
                                        <td>
                                            <button class="btn btn-primary" x-data @click="
                                                $('#showimage-modal').modal('toggle');
                                                document.getElementById('img-preview').src= '{{ asset('storage/barangs/'.$d->gambar) }}';
                                            ">View Image</button>
                                        </td>
                                        <td>
                                            <button
                                                class="btn btn-warning"
                                                wire:click="edit({{ $d->id }})"
                                                @click="$('#acc-modal').modal('toggle');"
                                            >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button
                                                class="btn btn-danger"
                                                wire:click="confirmDelete({{ $d->id }})"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100" class="text-center">
                                            No Data Found
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <div class="float-end">
                            {{ $get->links() }}
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    {{-- Create / Update Modal --}}
    <x-acc-modal title="{{ $isUpdate ? 'Update' : 'Create' }} {{ $title }}">
        <x-acc-form submit="saveWithUpload">
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Penanggung Jawab</label>
                    <select wire:model="form.pegawai_id" class="form-control">
                        <option value="">--Select Penanggung Jawab--</option>
                        @foreach($pegawai as $p)
                            <option value="{{ $p->id }}">{{ $p->nama }}</option>
                        @endforeach
                    </select>
                    <x-acc-input-error for="form.pegawai_id" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Nomer Urut</label>
                    <input type="text" wire:model="form.nomerurut" class="form-control" placeholder="Nomer Urut" readonly>
                    <x-acc-input-error for="form.nomerurut" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Serial Number</label>
                    <input type="text" wire:model="form.serialnumber" class="form-control" placeholder="Serial Number">
                    <x-acc-input-error for="form.serialnumber" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Nama</label>
                    <input type="text" wire:model="form.name" class="form-control" placeholder="Nama">
                    <x-acc-input-error for="form.name" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Merek</label>
                    <input type="text" wire:model="form.merek" class="form-control" placeholder="Merek">
                    <x-acc-input-error for="form.merek" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Ruangan</label>
                    <input type="text" wire:model="form.ruangan" class="form-control" placeholder="Ruangan">
                    <x-acc-input-error for="form.ruangan" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Model</label>
                    <input type="text" wire:model="form.model" class="form-control" placeholder="Model">
                    <x-acc-input-error for="form.model" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Harga</label>
                    <input type="number" wire:model="form.harga" class="form-control" placeholder="Harga">
                    <x-acc-input-error for="form.harga" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Tanggal Pengadaan</label>
                    <input type="date" wire:model="form.tanggal_pengadaan" class="form-control" placeholder="Tanggal Pengadaan">
                    <x-acc-input-error for="form.tanggal_pengadaan" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Kondisi</label>
                    <select wire:model="form.kondisi" class="form-control">
                        <option value="">--Select vendor--</option>
                        <option value="Baik">Baik</option>
                        <option value="Rusak Ringan">Rusak Ringan</option>
                        <option value="Rusak Berat">Rusak Berat</option>
                    </select>
                    <x-acc-input-error for="form.kondisi" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Vendor</label>
                    <input type="text" wire:model="form.vendor" class="form-control" placeholder="Vendor">
                    <x-acc-input-error for="form.vendor" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Image</label>
                    @if ($form->gambar || $image)
                        <div class="mb-3">
                            <img src="{{ $image ? $image->temporaryUrl() : url('storage/barangs/' . $form->gambar ?? $image) }}" alt="favicon" style="width: 100%">
                        </div>
                    @endif
                    <input type="file" wire:model="image" class="form-control">
                    <x-acc-input-error for="image" />
                </div>
            </div>
        </x-acc-form>
    </x-acc-modal>

    {{-- Show image --}}
    <x-acc-modal title="Preview Image" id="showimage-modal">
        <img src="" width="100%" id="img-preview">
    </x-acc-modal>

    {{-- Show QR --}}
    <x-acc-modal title="Preview QR" id="showqr-modal">
        <div id="img-qr">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        QR Code
                    </h1>
                    {!! \QrCode::size(300)->generate($qr) !!}
                    <br>
                    <a class="btn btn-primary" href="{{ route('export-qr', $form->id) }}" target="_blank">Download</a>
                </div>
                <div class="col-md-12">
                    <h1>
                        Barcode
                    </h1>
                    @php
                        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                    @endphp
                    <img src="data:image/png;base64,{{ base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128)) }}"
                        alt="Waduh"
                        style="width: 100%">
                    <br>
                    {{-- Alphine js click donwload img above --}}
                    <a class="btn btn-primary" href="{{ route('export-barcode', $form->id) }}" target="_blank">Download</a>
                </div>
            </div>
        </div>
    </x-acc-modal>
</div>
