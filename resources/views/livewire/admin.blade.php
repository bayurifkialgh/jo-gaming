<div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title ?? '' }}
        </h1>
    </section>

    <!-- Main content -->
    <section class="content" style="margin-top: 50px">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $title ?? '' }}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <x-acc-header />
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <x-acc-loop-th :$searchBy :$orderBy :$order />
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                @forelse($get as $d)
                                    <tr>
                                        <td>{{ $d->email }}</td>
                                        <td>{{ $d->name }}</td>
                                        <td>
                                            <button
                                                class="btn btn-warning"
                                                wire:click="edit({{ $d->id }})"
                                                @click="$('#acc-modal').modal('toggle');"
                                            >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button
                                                class="btn btn-danger"
                                                wire:click="confirmDelete({{ $d->id }})"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100" class="text-center">
                                            No Data Found
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <div class="float-end">
                            {{ $get->links() }}
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    {{-- Create / Update Modal --}}
    <x-acc-modal title="{{ $isUpdate ? 'Update' : 'Create' }} {{ $title }}">
        <x-acc-form submit="save">
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Email</label>
                    <input type="email" wire:model="form.email" class="form-control" placeholder="Email">
                    <x-acc-input-error for="form.email" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" wire:model="form.name" class="form-control" placeholder="Name">
                    <x-acc-input-error for="form.name" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Password</label>
                    <input type="password" wire:model="form.password" class="form-control" placeholder="Password">
                    <x-acc-input-error for="form.password" />
                </div>
            </div>
        </x-acc-form>
    </x-acc-modal>
</div>
