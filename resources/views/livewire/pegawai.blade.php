<div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $title ?? '' }}
        </h1>
    </section>

    <!-- Main content -->
    <section class="content" style="margin-top: 50px">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $title ?? '' }}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <x-acc-header />
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <x-acc-loop-th :$searchBy :$orderBy :$order />
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                @forelse($get as $d)
                                    <tr>
                                        <td>{{ $d->nrk }}</td>
                                        <td>{{ $d->nama }}</td>
                                        <td>{{ $d->unit }}</td>
                                        <td>
                                            <button
                                                class="btn btn-warning"
                                                wire:click="edit({{ $d->id }})"
                                                @click="$('#acc-modal').modal('toggle');"
                                            >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button
                                                class="btn btn-danger"
                                                wire:click="confirmDelete({{ $d->id }})"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="100" class="text-center">
                                            No Data Found
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <div class="float-end">
                            {{ $get->links() }}
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    {{-- Create / Update Modal --}}
    <x-acc-modal title="{{ $isUpdate ? 'Update' : 'Create' }} {{ $title }}">
        <x-acc-form submit="save">
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">NRK</label>
                    <input type="text" wire:model="form.nrk" class="form-control" placeholder="NRK">
                    <x-acc-input-error for="form.nrk" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Nama</label>
                    <input type="text" wire:model="form.nama" class="form-control" placeholder="Nama">
                    <x-acc-input-error for="form.nama" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="mb-3">
                    <label class="form-label">Unit</label>
                    <input type="text" wire:model="form.unit" class="form-control" placeholder="Unit">
                    <x-acc-input-error for="form.unit" />
                </div>
            </div>
        </x-acc-form>
    </x-acc-modal>
</div>
