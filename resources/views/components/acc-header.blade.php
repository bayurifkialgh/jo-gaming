@props(['isCreate' => true, 'isSearch' => true])

<div>
    <div class="row" style="margin-bottom: 25px;">
        @if($isSearch)
            <div class="col-md-{{ $isCreate ? '6' : '12' }}">
                <x-acc-search />
            </div>
        @endif
        @if($isCreate)
            <div class="col-md-{{ $isSearch ? '6' : '12' }}">
                <x-acc-create-btn />
            </div>
        @endif
        {{ $slot->isEmpty() ? '' : $slot }}
    </div>
</div>
