<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('template') }}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            {{-- <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i
                        class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ asset('template') }}/index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                    <li><a href="{{ asset('template') }}/index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                </ul>
            </li> --}}
            <li class="treeview">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ route('product') }}">
                    <i class="fa fa-shopping-cart"></i> <span>Product</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ route('pegawai') }}">
                    <i class="fa fa-user"></i> <span>Pegawai</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ route('admin') }}">
                    <i class="fa fa-user"></i> <span>Admin</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
