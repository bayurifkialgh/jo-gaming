<div x-data>
    <div class="text-right">
        <button
            class="btn btn-success"
            wire:loading.attr="disabled"
            wire:target="create"
            wire:click="create"
            @click="$('#acc-modal').modal('toggle')"
        >
            <i class="align-middle" data-feather="plus-circle"></i>
            Create
        </button>
    </div>
</div>
