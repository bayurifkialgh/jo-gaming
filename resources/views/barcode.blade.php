<h1>
    Barcode
</h1>
@php
    $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
@endphp
<img src="data:image/png;base64,{{ base64_encode($generator->getBarcode($barcode, $generator::TYPE_CODE_128)) }}"
    alt="Waduh"
    style="width: 100%">
<br>
