<table border="1">
    <thead>
        <th>Penanggung Jawab</th>
        <th>Nomer Urut</th>
        <th>Serial Number</th>
        <th>Nama</th>
        <th>Merek</th>
        <th>Ruangan</th>
        <th>Model</th>
        <th>Kondisi</th>
        <th>Vendor</th>
    </thead>
    <tbody>
        @forelse($get as $d)
            <tr>
                <td>{{ $d->pegawai }}</td>
                <td>{{ $d->nomerurut }}</td>
                <td>{{ $d->serialnumber }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->merek }}</td>
                <td>{{ $d->model }}</td>
                <td>{{ $d->ruangan }}</td>
                <td>{{ $d->kondisi }}</td>
                <td>{{ $d->vendor }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="100" class="text-center">
                    No Data Found
                </td>
            </tr>
        @endforelse
    </tbody>
</table>
