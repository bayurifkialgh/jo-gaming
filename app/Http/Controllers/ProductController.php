<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;

class ProductController extends Controller
{
    public function export() {
        $get = Barang::join('pegawais', 'pegawais.id', '=', 'barangs.pegawai_id')
                    ->select('barangs.*', 'pegawais.nama as pegawai')
                    ->get();

        $date = Carbon::now()->format('d-m-Y');

        return response()
                ->view('exports.excel', compact('get'))
                ->header('Content-Type', 'application/vnd.ms-excel')
                ->header('Content-Disposition', 'attachment; filename="'.$date.'_data_barang.xlsx"');

    }

    public function qr($id) {
        $get = Barang::find($id);
        $qr = "
            Nomer Urut : {$get->nomerurut}
            Serial Number : {$get->serialnumber}
            Nama Barang : {$get->name}
            Kondisi : {$get->kondisi}
            Tanggal Pengadaan : {$get->tanggal_pengadaan}
        ";

        return view('qr', compact('get', 'qr'));
    }

    public function barcode($id) {
        $get = Barang::find($id);
        $barcode = $get->serialnumber;

        $pdf = Pdf::loadView('barcode', compact('get', 'barcode'));
        return $pdf->download('barcode.pdf');

    }
}
