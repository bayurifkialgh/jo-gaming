<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $table = 'barangs';

    protected $fillable = [
        'nomerurut',
        'serialnumber',
        'name',
        'merek',
        'ruangan',
        'model',
        'harga',
        'tanggal_pengadaan',
        'kondisi',
        'gambar',
        'vendor',
    ];
}
