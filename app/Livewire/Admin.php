<?php

namespace App\Livewire;

use App\Livewire\Forms\FormAdmin;
use App\Models\User;

class Admin extends BaseComponent
{
    public FormAdmin $form;
    public $title = 'Admin';

    public $searchBy = [
        [
            'name' => 'Email',
            'field' => 'email',
        ],
        [
            'name' => 'Name',
            'field' => 'name',
        ],
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'name',
    $showImage = 0,
    $order = 'asc';

    public function render()
    {
        $get = $this->getDataWithFilter(new User, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }
        return view('livewire.admin', compact('get'))->title($this->title);
    }
}
