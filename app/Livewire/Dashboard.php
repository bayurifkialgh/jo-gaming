<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\Barang;

class Dashboard extends Component
{
    public function render()
    {
        $user = User::count();
        $barang = Barang::count();

        return view('livewire.dashboard', compact('user', 'barang'));
    }
}
