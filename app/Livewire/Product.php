<?php

namespace App\Livewire;

use App\Livewire\Forms\FormProduct;
use App\Models\Barang;
use App\Models\Pegawai;
use Livewire\WithFileUploads;

class Product extends BaseComponent
{
    use WithFileUploads;

    public FormProduct $form;
    public $title = 'Product';

    #[Rule('nullable|image:jpeg,png,jpg,svg|max:2048')]
    public $image;

    public $qr = 'waduh';
    public $barcode = 'waduh';

    public $searchBy = [
        [
            'name' => 'Penanggung Jawab',
            'field' => 'pegawais.nama',
        ],
        [
            'name' => 'Nomer Urut',
            'field' => 'barangs.nomerurut',
        ],
        [
            'name' => 'Serial Number',
            'field' => 'barangs.serialnumber',
        ],
        [
            'name' => 'Nama Barang',
            'field' => 'barangs.name',
        ],
        [
            'name' => 'Merek',
            'field' => 'barangs.merek',
        ],
        [
            'name' => 'Ruangan',
            'field' => 'barangs.ruangan',
        ],
        [
            'name' => 'Model',
            'field' => 'barangs.model',
        ],
        [
            'name' => 'Kondisi',
            'field' => 'barangs.kondisi',
        ],
        [
            'name' => 'Vendor',
            'field' => 'barangs.vendor',
        ],
        [
            'name' => 'Gambar',
            'field' => 'barangs.gambar',
            'no_search' => true,
        ],
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'nomerurut',
    $showImage = 0,
    $pegawai = [],
    $order = 'asc';

    public function mount() {
        $this->pegawai = Pegawai::all();
    }

    public function render()
    {
        $model = Barang::join('pegawais', 'pegawais.id', '=', 'barangs.pegawai_id')->select('barangs.*', 'pegawais.nama as pegawai');

        $get = $this->getDataWithFilter($model, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }
        return view('livewire.product', compact('get'))->title($this->title);
    }

    public function saveWithUpload() {
        $this->form->gambar = $this->image;
        $this->save();
    }

    public function generateQR($id) {
        $this->form->getDetail($id);

        $this->qr = "
            Nomer Urut : {$this->form->nomerurut}
            Serial Number : {$this->form->serialnumber}
            Nama Barang : {$this->form->name}
            Kondisi : {$this->form->kondisi}
            Tanggal Pengadaan : {$this->form->tanggal_pengadaan}
        ";

        $this->barcode = $this->form->serialnumber;
    }
}
