<?php

namespace App\Livewire\Forms;

use App\Models\Pegawai;
use Livewire\Attributes\Rule;
use Livewire\Form;

class FormPegawai extends Form
{
    #[Rule('nullable|numeric')]
    public $id = '';

    #[Rule('required')]
    public $nrk = '';

    #[Rule('required')]
    public $nama = '';

    #[Rule('required')]
    public $unit = '';

    // Get the data
    public function getDetail($id) {
        $data = Pegawai::find($id);

        $this->id = $data->id;
        $this->nrk = $data->nrk;
        $this->nama = $data->nama;
        $this->unit = $data->unit;
    }

    // Save the data
    public function save() {
        $this->validate();

        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }

        $this->reset();
    }

    // Store data
    public function store() {
        Pegawai::create($this->only([
            'nrk',
            'nama',
            'unit',
        ]));
    }

    // Update data
    public function update() {
        Pegawai::find($this->id)->update($this->all());
    }

    // Delete data
    public function delete($id) {
        Pegawai::find($id)->delete();
    }

    public function resetData() {
        $this->reset();
    }
}

