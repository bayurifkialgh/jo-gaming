<?php

namespace App\Livewire\Forms;

use App\Models\User;
use Livewire\Attributes\Rule;
use Livewire\Form;

class FormAdmin extends Form
{
    #[Rule('nullable|numeric')]
    public $id = '';

    #[Rule('required')]
    public $name = '';

    #[Rule('required')]
    public $email = '';

    #[Rule('required')]
    public $password = '';

    // Get the data
    public function getDetail($id) {
        $data = User::find($id);

        $this->id = $data->id;
        $this->name = $data->name;
        $this->email = $data->email;
    }

    // Save the data
    public function save() {
        $this->validate();

        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }

        $this->reset();
    }

    // Store data
    public function store() {
        User::create($this->only([
            'name',
            'email',
            'password',
        ]));
    }

    // Update data
    public function update() {
        User::find($this->id)->update($this->all());
    }

    // Delete data
    public function delete($id) {
        User::find($id)->delete();
    }

    public function resetData() {
        $this->reset();
    }
}

