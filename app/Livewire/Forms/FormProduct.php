<?php

namespace App\Livewire\Forms;

use App\Models\Barang;
use Livewire\Attributes\Rule;
use App\Traits\WithSaveFile;
use Livewire\Form;

class FormProduct extends Form
{
    use WithSaveFile;

    #[Rule('nullable|numeric')]
    public $id = '';

    #[Rule('required')]
    public $pegawai_id = '';

    #[Rule('required')]
    public $nomerurut = '';

    #[Rule('required')]
    public $serialnumber = '';

    #[Rule('required')]
    public $name = '';

    #[Rule('required')]
    public $merek = '';

    #[Rule('required')]
    public $ruangan = '';

    #[Rule('required')]
    public $model = '';

    #[Rule('required')]
    public $harga = '';

    #[Rule('required')]
    public $tanggal_pengadaan = '';

    #[Rule('required')]
    public $kondisi = '';

    #[Rule('required')]
    public $vendor = '';

    #[Rule('nullable|image:jpeg,png,jpg,svg|max:2048')]
    public $gambar;

    // Get the data
    public function getDetail($id) {
        $data = Barang::find($id);

        $this->id = $data->id;
        $this->pegawai_id = $data->pegawai_id;
        $this->nomerurut = $data->nomerurut;
        $this->serialnumber = $data->serialnumber;
        $this->name = $data->name;
        $this->merek = $data->merek;
        $this->ruangan = $data->ruangan;
        $this->model = $data->model;
        $this->harga = $data->harga;
        $this->tanggal_pengadaan = $data->tanggal_pengadaan;
        $this->kondisi = $data->kondisi;
        $this->vendor = $data->vendor;
        $this->gambar = $data->gambar;
    }

    // Save the data
    public function save() {
        $this->validate();

        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }

        $this->reset();
    }

    // Store data
    public function store() {
        $save_path = 'barangs';

        // Save image
        if($this->gambar) {
            $this->gambar = $this->saveFile($this->gambar, $save_path, $save_path)['filename'];
        } else {
            $this->gambar = '';
        }

        Barang::create($this->only([
            'pegawai_id',
            'nomerurut',
            'serialnumber',
            'name',
            'merek',
            'ruangan',
            'model',
            'harga',
            'tanggal_pengadaan',
            'kondisi',
            'vendor',
            'gambar',
        ]));
    }

    // Update data
    public function update() {
        $old = Barang::find($this->id);
        $save_path = 'barangs';

        // Save image
        if($this->gambar) {
            $this->gambar = $this->saveFile($this->gambar, $save_path, $save_path)['filename'];
        } else {
            $this->gambar = $old->gambar;
        }

        $old->update($this->all());
    }

    // Delete data
    public function delete($id) {
        Barang::find($id)->delete();
    }

    public function resetData() {
        $this->reset();

        // Get nomerurut
        $this->getNomerUrut();
    }

    public function getNomerUrut() {
        $get = Barang::orderBy('nomerurut', 'desc')->first();

        $nomerurut = 'SCIBDG-0001';

        if($get) {
            $nomerurut = explode('-', $get->nomerurut);
            $nomer = (int) $nomerurut[1] + 1;
            $nomerLength = strlen("$nomer");
            $stringNumber = '0000';
            $nomerurut = $nomerurut[0] . '-' . substr_replace($stringNumber, $nomer, strlen($stringNumber) - $nomerLength);
        }

        $this->nomerurut = $nomerurut;
    }
}

