<?php

namespace App\Livewire;

use App\Livewire\Forms\FormPegawai;
use App\Models\Pegawai as ModelsPegawai;

class Pegawai extends BaseComponent
{
    public FormPegawai $form;
    public $title = 'Product';

    public $searchBy = [
        [
            'name' => 'NRK',
            'field' => 'nrk',
        ],
        [
            'name' => 'Nama',
            'field' => 'nama',
        ],
        [
            'name' => 'Unit',
            'field' => 'unit',
        ],
    ],
    $isUpdate = false,
    $search = '',
    $paginate = 10,
    $orderBy = 'nama',
    $showImage = 0,
    $order = 'asc';

    public function render()
    {
        $get = $this->getDataWithFilter(new ModelsPegawai, [
            'orderBy' => $this->orderBy,
            'order' => $this->order,
            'paginate' => $this->paginate,
            's' => $this->search,
        ], $this->searchBy);

        if ($this->search != null) {
            $this->resetPage();
        }
        return view('livewire.pegawai', compact('get'))->title($this->title);
    }
}
