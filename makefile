setup:
	@make build
	@make up
	@make composer-install
	@make artisan-migrate
	@make artisan-seed
build:
	docker-compose build --no-cache --force-rm
stop:
	docker-compose stop
up:
	docker-compose up -d
exec:
	docker exec -it jo-mama bash
composer-install:
	docker exec jo-mama bash -c "composer install"
composer-update:
	docker exec jo-mama bash -c "composer update"
artisan-serve:
	docker exec jo-mama bash -c "php artisan serve --host=0.0.0.0"
artisan-migrate:
	docker exec jo-mama bash -c "php artisan migrate"
artisan-seed:
	docker exec jo-mama bash -c "php artisan db:seed"
